package android.training.learningbase

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        const val userReferencePath = "USERS"
    }

    private lateinit var database: DatabaseReference
    private var userChildEventListener = object : ChildEventListener {
        override fun onCancelled(p0: DatabaseError) {
            //
        }

        override fun onChildMoved(p0: DataSnapshot, p1: String?) {
            //
        }

        override fun onChildChanged(p0: DataSnapshot, p1: String?) {
            Log.d(
                "TEST",
                "USER TO BE SAVED IS: ${p0.getValue(UserModel::class.java)}"
            )
            val user = p0.getValue(UserModel::class.java)
            updateUI(user)
        }

        override fun onChildAdded(p0: DataSnapshot, p1: String?) {
            Log.d(
                "TEST",
                "INITIAL USER TO BE SAVED IS: ${p0.getValue(UserModel::class.java)}"
            )
            val user = p0.getValue(UserModel::class.java)
            updateUI(user)
        }

        override fun onChildRemoved(p0: DataSnapshot) {
            //
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        initFireBase()
        setupAddButton()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        removeListeners()
        super.onDestroy()
    }

    private fun initFireBase() {
        database = FirebaseDatabase.getInstance().reference
        database.addChildEventListener(userChildEventListener)
    }

    private fun setupAddButton() {
        add_user_button.setOnClickListener {
            val user: UserModel =
                if (first_name_edit.length() > 0 && last_name_edit.length() > 0) UserModel(
                    first_name_edit.text.toString(),
                    last_name_edit.text.toString()
                )
                else UserModel()
            database.child(userReferencePath).setValue(user)
            showToast(user)
        }
    }

    private fun showToast(user: UserModel) {
        Toast.makeText(this, user.toString(), Toast.LENGTH_LONG ).show()
    }

    private fun updateUI(user : UserModel?){
        last_name_added.text = getString(R.string.last_name_added, user?.name, user?.lastName)
    }

    private fun removeListeners(){
        database.removeEventListener(userChildEventListener)
    }
}
