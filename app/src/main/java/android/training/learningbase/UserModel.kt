package android.training.learningbase

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
data class UserModel(var name: String = "", var lastName: String = "")

